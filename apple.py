"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""
import termcolor

def main():
    """Add "яблоко" word."""
    n = int(input("Сколько яблок Вам нужно?\n"))
    if n == 0 or 5 <= n <= 20 or 25 <= n <=30:
        print("Пожалуйста,", n, "яблок")
    elif n == 1 or n==21:
        print("Пожалуйста,", n, "яблоко")
    elif 2 <= n <= 4 or 22 <= n <=24:
        print("Пожалуйста,", n, "яблока")
    else:
        print("Столько нет")

if __name__ == "__main__":
    print(termcolor.colored("Grocery assistant","red"))  # color this caption
    main()
